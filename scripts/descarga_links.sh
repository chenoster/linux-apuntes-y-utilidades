#!/bin/bash
# ------------------------------------------------------------------
# [Author] @chenoster - https://gitlab.com/chenoster
#          Descarga todos los enlaces de una web
#          Este script está bajo licencia GNU GENERAL PUBLIC LICENSE
# ------------------------------------------------------------------

# VERSION=1.0
# USO="Descarga el script, dale permisos de ejecución y ejecuta: ./descarga_links.sh <URL>"

wget -O html $1

cat html | grep href > enlaces

while IFS= read -r linea
do

echo $linea | sed -r 's/.*href="([^"]+).*/\1/g' >> ficheros

done < enlaces

while IFS= read -r linea
do

wget $1$linea

done < ficheros

rm ficheros
rm enlaces
rm html