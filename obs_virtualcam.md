# Instalar OBS-Studio con VirtualCam en Debian y derivados

A partir de la versión 26 OBS-Studio soporta cámara virtual de manera nativa pero ¡Sólo para Windows! :(

Instalarlo en Linux es un poco más complicado pero siguiendo estos pasos lo lograremos:

### Primero tenemos que instalar la cámara virtual a nivel de sistema operativo
~~~
sudo apt install v4l-utils v4l2loopback-utils v4l2loopback-dkms
sudo modprobe v4l2loopback devices=1 card_label="loopback 1" exclusive_caps=1,1,1,1,1,1,1,1
~~~

### Instalamos paquetes necesarios, tendremos que compilar obs y el plugin VirtualCam
~~~
sudo apt-get install \
             build-essential \
             checkinstall \
             cmake \
             git \
             libmbedtls-dev \
             libasound2-dev \
             libavcodec-dev \
             libavdevice-dev \
             libavfilter-dev \
             libavformat-dev \
             libavutil-dev \
             libcurl4-openssl-dev \
             libfdk-aac-dev \
             libfontconfig-dev \
             libfreetype6-dev \
             libgl1-mesa-dev \
             libjack-jackd2-dev \
             libjansson-dev \
             libluajit-5.1-dev \
             libpulse-dev \
             libqt5x11extras5-dev \
             libspeexdsp-dev \
             libswresample-dev \
             libswscale-dev \
             libudev-dev \
             libv4l-dev \
             libvlc-dev \
             libx11-dev \
             libx264-dev \
             libxcb-shm0-dev \
             libxcb-xinerama0-dev \
             libxcomposite-dev \
             libxinerama-dev \
             pkg-config \
             python3-dev \
             qtbase5-dev \
             libqt5svg5-dev \
             swig \
             libxcb-randr0-dev \
             libxcb-xfixes0-dev \
             libx11-xcb-dev \
             libxcb1-dev \
             libxss-dev
~~~

### Clonamos el repositorio de OBS-Studio, compilamos e instalamos            
~~~
git clone --recursive https://github.com/obsproject/obs-studio.git
cd obs-studio
mkdir build && cd build
cmake -DUNIX_STRUCTURE=1 -DCMAKE_INSTALL_PREFIX=/usr ..
make -j4
sudo checkinstall --default --pkgname=obs-studio --fstrans=no --backup=no --pkgversion="$(date +%Y%m%d)-git" --deldoc=yes
~~~

### Clonamos el repositorio del plugin, compilamos e instalamos
~~~
git clone https://github.com/CatxFish/obs-v4l2sink.git
cd obs-v4l2sink
mkdir build && cd build
cmake -DLIBOBS_LIB="/home/usuario/obs-studio/libobs" -DLIBOBS_INCLUDE_DIR="/home/usuario/obs-studio/libobs" -DCMAKE_INSTALL_PREFIX=/usr ..
make -j4
sudo make install
~~~

### Para que el módulo v4l2loopback cargue automáticamente al arrancar
~~~
/etc/modules
Añadir linea: 
v4l2loopback

/etc/modules-load.d/
Añadir fichero v4l2loopback.conf con el texto: 
options v4l2loopback devices=1 card_label="loopback 1" exclusive_caps=1,1,1,1,1,1,1,1 

cd /etc/modprobe.d/
sudo cp /etc/modules-load.d/v4l2loopback.conf .

update-initramfs -c -k $(uname -r)
~~~