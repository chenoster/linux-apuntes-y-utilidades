## Servidor de correo en 2 minutos con docker y poste.io

`apt-get update`

`apt-get install docker.io docker-compose`

`docker run -p 25:25 -p 80:80 -p 443:443 -p 110:110 -p 143:143 -p 465:465 -p 587:587 -p 993:993 -p 995:995 -e TZ=Europe/Madrid -v /home/usuario/mail:/data -t --restart unless-stopped analogic/poste.io`

Y ya está. Sólo hay que ir a la configuración del dominio y añadir los siguientes registros:

A: mail.midominio.com  ip_del_servidor_de_correo

MX: midominio.com mail.midominio.com prioridad 10

#### Opcional pero muy recomendado es crear más registros de DNS:

Ver DKIM, código de verificación de google, v=spf1 mx ~all, ...

Más información sobre configuración y registros DNS:

https://wiki.zimbra.com/wiki/Best_Practices_on_Email_Protection:_SPF,_DKIM_and_DMARC