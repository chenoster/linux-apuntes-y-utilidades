## Dislocker (bitlocker en linux)

Ahora que Dislocker está instalado, debemos configurarlo. Para comenzar, debemos de crear una nueva carpeta de montaje donde se pueda acceder a la partición de Bitlocker cuando se complete el proceso de descifrado.

sudo mkdir -p /media/contenido

Después de crear la carpeta “contenido” en el directorio de medios, se debe crear otra carpeta. 

sudo mkdir -p /media/bitlocker/

Una vez que ambas carpetas están en el directorio de medios, puede verificar si las carpetas están donde se supone que deben ya sea moviéndose al directorio con CD o desde su gestor de archivos.

Ahora es el momento de usar Dislocker para descifrar y montar la partición de Bitlocker desde Linux.

Para ello, lo primero que debemos hacer es ejecutar el comando lsblk para encontrar la etiqueta de la partición de Windows que esta cifrada con Bitlocker.

lsblk

Que en mi caso sera /dev/sdb1, pero en cualquier caso puede diferir, es por ello que es importante identificar la partición.

Ya identificado, ahora utilizaremos dislocker, descifra la partición. Para ello vamos a teclear el siguiente comando colocando la etiqueta de la partición y tambien colocando la ruta de la carpeta que creamos con el nombre de “bitlocker” anteriormente.

sudo dislocker -V /dev/sdb1 -u -- /media/bitlocker

Aqui se nos pedirá la contraseña utilizada para acceder al usuario de Windows. La tecleamos y pulsamos enter.

Usando el comando dislocker, ahora toca montar la partición de Bitlocker en la carpeta /media/contenido.

sudo mount -o loop /media/bitlocker/dislocker-file /media/contenido

Es importante tomar en cuenta que Dislocker montará automáticamente su partición en modo de “solo lectura” y que para solucionar el problema basta con ejecutar un chkdsk en la unidad.

Ya con ello podremos acceder al contenido de la partición cifrada con Bitlocker desde Linux y poder trabajar con los archivos dentro de ella.

Finalmente para desmontar la partición de Bitlocker en Linux, basta con ejecutar el comando umount sobre la carpeta. Es importante mencionar que se debe cerrar todo documento o archivo con el que sé este trabajando de esta partición.

Para desmontar la partición basta con teclear:

sudo umount /media/contenido

En caso de que la partición no se desmonte, podremos forzar el desmontaje tecleando el siguiente comando:

sudo umount /media/contenido -f