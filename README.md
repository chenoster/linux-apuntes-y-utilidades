# Apuntes y scripts útiles de Linux

· [Ver Apuntes](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/tree/master/apuntes)  
· [Ver Scripts](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/tree/master/scripts)  

--------------------------------

### Otras notas:

· [Conecta por SSH sin tener que escribir la contraseña](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/ssh_sin_contrasena.md)

· [Utiliza discos cifrados con bitlocker en linux](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/bitlocker_linux.md)

· [Instala OBS-Studio con VirtualCam en debian y derivados](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/obs_virtualcam.md)

· [Servidor de correo en 2 minutos](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/servidor_correo_en2min.md)

· [Notas cortas variadas](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/notas_cortas_variadas.md)