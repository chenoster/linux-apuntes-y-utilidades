### USO DE MAN
man: (Manual) Muestra las páginas del manual del comando que le pasamos como parámetro  
man -d (debug) Muestra información de debug  
man -k Nos muestra en qué archivos de man aparece esa palabra  
man -w Nos muestra la ruta del manual  
man -a (all) Nos muestra todas las páginas del manual  
man -aw Nos muestra las rutas de todas las páginas del manual  
man (archivos o páginas de manual)  
man 1 Programas de usuario  
man 2 Llamadas de sistema  
man 3 Llamadas a librerías  
man 4 Archivos especiales con los que trabaja este programa  
man 5 Formatos de archivo  
man 6 Juegos  
man 7 Información varia o miscelania  
man 8 Información del sistema  
man (variable de entorno) $MANSECT (se define el orden de las páginas)  
man (configuración) /etc/mampath.config  

apropos Busca comandos relacionados utilizando las palabras clave del manual

Simplified and community-driven man pages  
https://tldr.sh/

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)