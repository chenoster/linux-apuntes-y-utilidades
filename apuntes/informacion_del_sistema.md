### INFORMACIÓN DEL SISTEMA

acpi: Muestra información de la batería y sensores térmicos  
acpi -V (everything) Muestra información de todo (ventiladores, batería, ...)  

cal: (Calendario) Muestra el calendario. Por defecto muestra el mes actual  
cal Muestra el calendario de todo el año que se le indica por parámetro  
cal Muestra el calendario del mes que se le indica por parámetro  

date: (Fecha) Muestra la fecha y hora actual  

dmesg: Muestra el log de arranque y apagado del equipo  

lsdev: (Lista dispositivos) Nos muestra todos los dispositivos conectados  

lsmod: Listar modulos del sistema que tenemos disponibles  

lspci: (Lista PCI) Lista todo el hardware conectado por PCI  

lsusb: (Lista USB) Lista todos los dispositivos USB  

uname  


[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)