## BASH SCRIPTING

~~~
Shebang: linea especial que define que tipo de intérprete usar para un script.
#! /bin/bash
#! /usr/bin/perl
Limitar la ejecución de scripts con chmod
Herencia entre la terminal y los scripts
Uso de SUID y GUID en un script

Condicionales if/then/else/elif/fi, son usados para definir condiciones en un programa.
if [ linux_is_cool eq true ] then
echo "Yeah!"
fi
Condicional case: es usado para probar una cadena con un numero finito de coincidencias.
case cadena in
pattern1)
commands
;;
pattern2)
commands
;;
esac
Comando getops: validar las opciones que son pasadas como parámetros a un script.

for: función que realiza loops en función al número de elementos que contenga la lista con la que se define la sentencia.
for x in lista
do
comando
done

while: realiza un loop mientras que una condición sea verdadera.
while
comando_prueba
do
comando
done

until: realiza un loop mientras una condición sea falsa.
until
comando_prueba
do
comando
done

break: sale del ciclo anidado con más profundidad.

continue: es un atajo para ir directamente a la nueva iteración de un ciclo.

exit: permite salir de un programa definiendo un número de salida.

Comando function: permite definir un nombre de función que puede recibir parámetros.
function name
{
comando
}
return: es usado dentro de una funcion para devolver un valor
Comando kill: permite enviar señales a programas:
-l, listar las señales disponibles.
-s N, permite enviar una señal en específico con el numero N

Operador read: permite leer una linea de la entrada estándar y la asigna a una o más variables

Operador shift: usado para mover de posición los parámetros recibidos por la cónsola

Comando source: usado para ejecutar las líneas de un archivo
~~~

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)