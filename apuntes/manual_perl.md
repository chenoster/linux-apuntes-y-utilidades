## MANUAL DE PERL

~~~
Los scripts de Perl siempre empiezan con #! /usr/bin/perl
Si no llevan la linea anterior al principio se podría ejecutar con el comando perl <nombre fichero.pl>

print "Hola Mundo\n";

En el último comando del archivo no hace falta poner el ;

Perl no interpreta los espacios en blanco, podemos poner los que queramos que no afecta, podemos usarlos para identar

if(1){
}
else{
}

#Separador, perl lo ignora, nos puede ayudar a hacer el código más legible
32000 = 32_000 = 3_2000

# Esto es un comentario de una linea en Perl

=for comment   
Comentario de varias lineas
=cut

sub funcion(){
   return x;
}

Documentación de perl se puede ver con el comando perldoc

CPAN cpan.org Almacena módulos gratuitos para Perl muy útiles y podemos buscar documentación sobre ellos

Se usa my para declarar variables:

my $VARIABLE=valor;
my @LISTA=(1,2,3);
my %HASH=(1=>"x");

Se recomienda poner esto al principio del script:
use warnings;
use strict;

' ' Cadenas literales de texto. No permite colocar variables dentro, muestra el contenido literalmente sin interpretar nada
" " Si permite colocar variables, saltos de linea, etc
\ Caracter de escape 

q{texto} Equivale a comillas simples
qq{texto} Equivale a comillas dobles

El punto . sirve para concatenar cadenas

Añadir elementos a un array push(@array, 1); Añade al final.  unshift(@array, 1); Añade el elemento nuevo al principio del array

Quitar elementos de un array pop(@array); Devuelve el último elemento del array y lo elimina
shift(@array) lo mismo que pop pero empieza por el primer elemento

my %hash = (
	"gato" => "blanco",
	"perro"=> "marron",
	"pajaro"=> "amarillo"
);

#%hash->{"caballo"} = "negro";
#delete $hash{"caballo"};

#print join(" ", values(%hash));
foreach my $var (sort(keys(%hash))) {
	print "Key: $var, value:".$hash{$var}."\n";
}

sort(); Ordena alfabeticamente

if(defined $variable){
print "Defined nos indica que la variable tiene algún valor, está definida";
}

$variable = undef; Para definir variables sin valor explicitamente (no es necesario ponerlo) o para quitar el valor a una variable -> undef($variable);

my @array = (); Creamos un array vacío
my @array = (1,2,3,4) o my @array = qw(1 2 3 4)  o  my @array = qw/1 2 3 4/  Sirve cualquier caracter

print join(" ", @array); Muestra todo el array y separa con la cadena del primer parámetro

$array[0] se hace referencia a los elementos del array así, empieza desde el 0

($a, $b, $c) = @array; asignamos las tres primeras posiciones del array a las variables a, b y c

scalar @array nos dice el número de elementos que tiene nuestro array

@array[0 .. 2] de la posición 0 al 2
@array[0 .. 2, 4 .. 7]

CONDICIONAL
if($a == 4){
   print "$a es igual a 4";
}
elsif($a == 5){
   print "$a es igual a 5";
}
else{
   print "$a no es igual a 4 ni a 5";
}
En una sola linea: print "es igual" if($a == 5);

unless($a ==5){} # Lo contrario que if, sólo se cumple si la condición es falsa

print "Estamos dentro" unless($a !=5);

Condicional ternario
my $a = 44;
my $b = ($a > 50) ? 1 : 0;
print "Valor de b: $b \n";

++$a suma 1 antes de realizar la acción. $a++ Realiza la acción y luego suma 1

$a = $a * $b   es lo mismo que  $a *= $b

Los operadores de comparación numérica < <= == >= > !=
Los operadores de comparación de cadenas son eq (igual) ne (no igual) gt (greater mayor que) lt(menor que) le (menor o igual) ge (mayor o igual)

Operadores lógicos:    && AND     || OR  (se pueden usar indistintamente las palabras and/or y los símbolos && ||)

# Si la variable $inicio es falsa (no tiene valor), entonces $a = 123
my $inicio = undef;
my $a = $inicio || 123;


=for comment
	Operadores de archivos:
	my @lista = <*>
	-e valida si un archivo existe
	-f valida que la ruta es de un archivo
	-d valida que la ruta es un directorio
	-s retorna el tamaño del archivo en bytes
Existen muchos más, se pueden buscar en la documentación: comando perldoc
=cut


print join(":", <c*>) #Nos muestra todos los ficheros que comienzan por c que hay en la carpeta

#print join("\n", );
if( -d "cond_ternario.pl" ){
	print "Verdadero";
}
else{
	print "Falso";
}

#bucles:
while (condicion) {}

#$_ siempre toma el último valor de la última función o sentencia ejecutada
print $_ while (<*>);

until (condicion) {}  # Mientras no se cumpla la condición haz esto (lo contrario que while)

print "Valor de $a" until (++$a > 5);

foreach my $var (@array) { print "El valor es: $var \n"; }

foreach (@array) { print "El valor es: $_ \n"; }

print $_." "  foreach (@array);

# Comandos especiales para bucles:
next para saltar una iteración
last para salir del bucle

#Expresiones regulares
. Cualquier caracter
^ Inicio de la cadena
$ Final de la cadena
| OR
\ Escapar caracter
[] Rango
() Agrupar logicamente

#Expresiones regulares - Operadores de cantidades
* Cero o más
+ Uno o más
? Cero o uno
{m,n} Cuantificador manual

#Expresiones regulares - Definidos por Perl
\w Palabras (números, letras, ...) 
\W Caracteres que no forman palabras
\s Espacios en blanco
\S Caracteres que no son espacios
\d Caracteres numéricos
\D Caracteres que no son numéricos

open(ARCHIVO, "texto.txt");
while (my $line = ){
	if($line =~ m/'([a-zA-Z .]+)'/){
   	print $line
    }
}
close(ARCHIVO);

open(ARCHIVO, "texto.txt");
while (my $line = ){
	if($line =~ s/a/b/){
   	print $line
    }
}
close(ARCHIVO);

open(ARCHIVO, "texto.txt");
while (my $line = ){
	if($line =~ m/(^..).+(..$)/){
   	print $1."  ".$2."\n";
    }
}
close(ARCHIVO);

=for comment
	Expresiones regulares
	Comando split: usado para separar cadenas
=cut

my $cadena = "hola, como, estas"
my @array = split(",", $cadena);

my $cadena = "10-01-1678 12:12:12";
my @array = split(/[- :]/, $cadena);
print join(",", @array); 

# Funciones
sub imprimir {
	print "mensaje";
}

imprimir(); #Llamamos a la función

Cómo adquirir parámetros dentro de una función:
my ($a, $b, $c) = @_;

Retornando valores usando la comando return:
return "valor de retorno";

# Manejo de ficheros
La sintaxis principal para abrir un puntero FH a un archivo:

comando open(FH, '<', "miarchivo.txt") || die("no se puede abrir archivo el error es $!");

Los principales tipos de modificadores de acceso son:
<, abrir el archivo para lectura.
>, abre el archivo para escritura, crea el archivo si no existe y reinicia el archivo si existe.
>>, para concatenar al final de un archivo existente, crea un nuevo archivo si no existe
+<, abre el archivo para escritura y lectura.

Para abrir e imprimir un archivo podemos usar la siguiente sintaxis:

open(ARCHIVO, "<", "texto.txt") || die("no se puede abrir archivo $!");
while (my $line = ){
	if($line =~ s/e/BBB/){
   	print $line;
    }
}
close(ARCHIVO);

Para editar un archivo podemos usar la siguiente sintaxis:

open(ARCHIVO2,">", "texto2.txt") || die("no se puede abrir archivo $!"); #$! Devuelve el último error que se produjo
print ARCHIVO2 "Hola!";
close(ARCHIVO2);

Ejemplo:

#! /usr/bin/perl

use warnings;
use strict;

=for comment
	Trabajar con archivos
	Abrir un filehandle = open(MIARCHIVO, '<', "miarchivo.txt");
=cut

open(MIARCH, '<', "texto.txt") || die("Error! $!");

while (my $linea = ) {
	print "$linea";
}

#print MIARCH "Esto es un texto nuevo!!\n";

close(MIARCH);

# MODULOS
Para buscar módulos está la página cpan.org , donde se pueden ver módulos de terceros existentes y ver su documentación

Para instalar un módulo podemos usar el comando: sudo cpan <nombre del módulo>

Con perldoc <nombre del módulo> podemos ver en la consola la documentación del módulo

Para usar o llamar un módulo podemos usar la sentencia use dentro del script


Tipo objeto:

use Digest::MD5;
my $ctx = Digest::MD5->new;
$ctx->add("mensaje!");
print $ctx->hexdigest;

Procedural:

use Digest::MD5 qw(md5_hex);
print md5_hex("mensaje!");

Ejemplo:

#! /usr/bin/perl

use warnings;
use strict;

=for comment
	Modulos
	Llamar modulos con use
=cut

use Digest::MD5 qw/ md5_hex /;

print md5_hex("erferferfr");

#my $dig = Digest::MD5->new;
#$dig->add("texto12345");
#print $dig->hexdigest();
~~~

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)