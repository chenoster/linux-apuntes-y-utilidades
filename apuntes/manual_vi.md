## MANUAL DE VI

Modos de operación de vi:  
Modo de comandos  
Modo de inserción  
Para entrar en este modo presionar i  
Para salir presionar esc  
Para moverse dentro de vi usar las flechas de dirección o usar j k h l  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)