### GESTIÓN DE FICHEROS Y DIRECTORIOS

bunzip2 Para descomprimir, igual que bzip2 -d  

bzip2 <nombre_archivo> Para comprimir archivos  
bzip2 -d <nombre_archivo> Descomprime un archivo  
bzip2 -1 (de -1 a -9, el 9 es la compresión más fuerte)  
bzip2 -c > <nombre_archivo_comprimido> Para mantener el fichero original y crear uno comprimido  

pwd: (Print W Directory) Muestra el directorio actual en el que nos encontramos  

cd: (Change Directory) Navegar entre directorios  
cd .. Va al directorio padre del actual  
cd ~ Nos lleva al directorio raíz del usuario /home/xxxx/  
cd Va al directorio que se le indica  

mkdir: Sirve para crear directorios  
mkdir -m777 Para asignar los permisos al crear la carpeta  
mkdir -p /xxx/xxx2/xxx3 Crea una carpeta y las carpetas padre si no existen  

rm: (Remove) Borra ficheros o carpetas  
rm -R (Recursive) Para borrar carpetas y su contenido  
rm -i (interactive) Nos pregunta antes de eliminar  
rm -f (force) No nos preguntará nada  

rmdir: Para eliminar carpetas  
rmdir -p (parent) Elimina también las carpetas padre si están vacías  

cp: (Copy) Copia ficheros o directorios  
cp -i (Modo interactivo) Nos pregunta si deseamos sobreescribir el archivo  
cp -f (forzar) No nos preguntará nada y sobreescribirá el archivo si ya existe  
cp -p (preserve) Preserva información (dueño, grupo, permisos, ...)  
cp -r (recursive) Para copiar carpetas  
cp -v (verbose) Muestra información de debug  
cp -d Realiza copias manteniendo los links  

mv: (Move) Mueve ficheros o directorios, también se utiliza para renombrar  
mv -i (interactive) Pregunta en caso de sobreescribir un archivo  

file Nos muestra el tipo de archivo  
file -f (para una lista de archivos)  
file -z (para archivos dentro de fichero comprimido)  

cpio: Sirve para empaquetar y desempaquetar en formato cpio  
ls | cpio - o > cpio_archivos.cpio  
cpio -iv < cpio_archivos.cpio  

dd: Copia por bloques y puede convertir ficheros (hacer cosa como pasar de mayúsculas a minúsculas). También se utiliza para copiar imágenes de disco y para formatear a bajo nivel  
dd if= of= (if input file, of output file)  
dd if=/dev/zero of=/dev/sdxxx Formatea a bajo nivel, pone todos los bits a 0  

ln: (Link) Crea enlaces simbólicos (suave, puntero hacia un archivo o carpeta) o enlaces duros  
ln -s (Soft) Crea un enlace simbólico, es como un acceso directo de Windows. Se pueden usar entre sistemas de ficheros diferentes.  
ln (hard link) Es otra entrada a un inodo que ya existe. Dentro de un sistema de ficheros.  

find / -lname archivo Para buscar links (enlaces suaves)  
find / -inum 123 Buscar por inodo, para encontrar hard links (enlaces duros)  

locate: Nos permite encontrar archivos rápidamente en la base de datos de ficheros (habrá que actualizarla si el fichero es nuevo, updatedb, permite excluir carpetas con el modificador -e o en su archivo de configuración)  

touch: Sirve para crear un fichero vacío y también para modificar metadatos, por ejemplo, la última fecha de acceso o de modificación del fichero  
touch -a Para cambiar la fecha de acceso  
touch -m Para cambiar la fecha de modificación  
touch -t (timestamp) Para especificar la fecha YYYYMMDDHHMM  

tar -cvzf (create new file, verbose, comprimir, file siguiente parámetro) Para empaquetar una carpeta  
tar -xvzf (extract, verbose, descomprime, el fichero ...) Para extraer la carpeta comprimida y empaquetada  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)