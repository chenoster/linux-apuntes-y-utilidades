## CONFIGURACIÓN Y USO DE LA CONSOLA DE COMANDOS O TERMINAL

alias: Sirve para crear alias personalizados de comandos. Se puede añadir al fichero de personalización de bash ~/.bashrc para que sea permanente  
alias ll='ls -l' Para eliminar el alias unalias ll.  
ls: (List) Lista ficheros y directorios  
ls -l (List) Muestra ficheros y directorios en forma de lista, con permisos, fecha de modificación y tamaño  
ls -lh (List Human) Salida en forma de lista y el tamaño de ficheros legible para humanos  
ls -a (All) Muestra también ficheros y carpetas ocultos  
ls -A Excluye los directorios . y .. (útil para cuando utilizamos el comando dentro de un script)  
alias ll='ls -lh'  
alias la='ls -lha'  
alias lls='ls -lh | less'  
alias las='ls -lha | less'  

unalias : Para eliminar un alias  

clear: Limpia la pantalla  

Flujos de datos de la consola:  
Entrada estándar (stdin), definido por el número 0  
Salida estándar (stdout), definido por el número 1  
Error estándar (stderr), definido por el número 2  

Tuberías y redirecciones:  
ls | less La salida de ls es la entrada de less  
1> archivo, redireccionar 1 para un nuevo archivo  
2> archivo, redireccionar 2 para un nuevo archivo  
2>&1, redireccionar 2 para 1  
1>> archivo, concatenar la información de 1 a un archivo  
2>> archivo, concatenar la información de 2 a un archivo  

tee: Comando tee para escribir a un archivo el resultado de un comando y mostrarlo en pantalla. 
ps | grep command | tee archivo_texto | cut -d" " -f2  

xargs: Comando xargs para direccionar stdin en comandos que no lo soporten y para dividir entradas de texto muy grandes para la consola  
-t, permite ver el comando resultante antes de ejecutarlo  
-nN, permite definir la cantidad de parámetros pasados en cada comando  
-p, nos pregunta antes de ejecutar el comando  
ls | echo // no funciona  
ls | xargs echo //si funciona  

Ejecuta varios comandos:  
; Se ejecuta el primer comando y después el otro  
& Se ejecutan los dos comandos a la vez  
&& (AND) Si el primero termina con errores el segundo no se ejecuta  
|| (OR) Si el primero falla se ejecuta el segundo, si no sólo se ejecuta el primero  

exit: Para cerrar la sesión de terminal, cierra el terminal si estamos en entorno gráfico  

history: Histórico de comandos  
HISTSIZE: Variable que indica el tamaño del historial de comandos que se va a guardar      
HISTFILE: Variable que indica la ruta del fichero donde se guarda el historial de comandos en texto plano  
!! Ejecuta directamente el último comando  
!402 Ejecuta el comando 402 del histórico  
!-4 Ejecuta el cuarto comando anterior  
!les Ejecuta el último comando que empieza por la cadena les  
!?les Ejecuta el primer comando que comienza la cadena les  
^etc^tmp Ejecuta el último comando sustituyendo etc por tmp en el texto del comando  
ctrol+r: Busca comandos que empiecen por una letra (aparece un prompt donde te pide la letra)  

for var in $(comando); do accion; done  
for var in $(ls); echo $var; done  

whereis Nos muestra la ruta del comando y de sus manuales  

which Muestra la ruta del programa  

Valores de retorno:  

Los valores que retorna todo script o programa ejecutado desde un terminal.  
Retorna 0 cuando el programa terminó con éxito  
Retorna != 0 cuando el programa terminó con error  

Verificar el retorno de un programa en shell con la variable $?  

Detectar errores en scripts usando una condición y $?  

Comando test: permite calcular comparaciones e generar informaciones de archivos  
sustituido por [ expresión ] en un script  

test 'aaa' = 'aaa' (no devuelve nada, tenemos que hacer un echo $? para ver si la condición se cumple cuando sea = 0 o no se cumple)  
test -e archivo (Para verificar si un archivo existe)  

Sustitución de comandos con:  
$( comando )  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)