### GESTIÓN DE DISCOS

df: (Disk Free) Lista discos y particiones y el tamaño total, usado y libre de cada uno de ellos  
df -h (Human) Muestra los tamaños en unidades comunes (KB, MB, GB)  
df -i (Inode) Un sistema de archivos tiene un número limitado de inodos, si se agotan no se podrán crear más ficheros, este comando nos da información sobre el uso de inodos  

du: (Disk Usage) Saca un listado de carpetas y subcarpetas desde el directorio actual mostrando su tamaño  
du -h (Human) Muestra el tamaño en unidades comunes (KB, MB, GB)  
du -csh Nos dirá el tamaño que ocupa la carpeta  
du -a Muestra todos los archivos que no son directorios y su tamaño  
du -c Muestra el total de archivos listados  
du -s Muestra la suma total de tamaño  

fdisk: Utilidad para gestión de discos y particiones (Requiere root)  
fdisk -l (List) Lista discos y particiones  
fdisk /dev/sdb (entramos en modo interactivo)  
Para editar particiones el disco debe estar desmontado  
m: para obtener ayuda  
p: nos imprime la tabla de particiones  
n: nueva partición  
a: cambiar el flag de booteo a una partición  
t: definir el tipo de partición  

mkswap /deb/sdb1: Formatea una partición para ser usada como swap  
swapon -s: Lista las particiones que estamos usando como swap  
swapon /deb/sdb1: Para activar el uso de la partición como swap  
swapoff /deb/sdb1: Para desactivar el uso de una partición como swap  

mkfs: Permite definir el tipo de partición  
mkfs -t ext2 /deb/sdb1  
mkfs -t vfat /deb/sdb1 (para fat32)  

mount: Monta discos, particiones o sistemas de ficheros en la carpeta que se le indique, sin parámetros muestra dispositivos montados  
mount (sin parámetros) Nos muestra los dispositivos montados  
mount <carpeta_montaje>  
mount (fichero particiones montadas) /etc/mtab  

umount: Desmonta dispositivos montados, como parámetro admite tanto el nombre del dispositivo como la carpeta en la que se encuntra montado  
umount -a: Desmonta todas las particiones descritas en /etc/mtab que serán todas, si están en uso no podrá  
umount -t xfs -a: Desmonta todas las particiones de un tipo determinado  

Montar particiones automáticamente  
/etc/fstab  
En este fichero definimos el sistema de ficheros, dónde se va a montar y otros parámetros y permisos  

Discos IDE:  
/dev/hda (master)  
/dev/hdb (slave)  

Discos SATA o SCSI:  
/dev/sda (primer disco duro)  
/dev/sdb (segundo disco duro)  

particiones: Máximo 4 particiones primarias. Si una de ellas la convertimos en particion extendida, dentro de ella podemos tener hasta 15 particiones lógicas  

Las particiones lógicas se enumeran a partir de 5 /dev/sda5  

particiones para ordenador personal:  
/boot sistema de arranque 500MB  
/root programas y librerías del sistema 2GB  
swap el doble de la memoria RAM  
/home el resto del disco duro  
particiones para servidor:  
/boot  
/root  
swap  
/home  
/var donde se guardan logs y otros mensajes del sistema  
/user programas de usuario (opcional)  

Inode: es una estructura de datos propia de los sistemas de archivos que contiene características que definen a un archivo menos su nombre. Es identificado por un número entero único  
df -i, para ver los inodos de un sistema de archivos  

Journaling: es una característica de algunos sistemás de archivos, la cual mantiene un log o journal en donde se almacenan los cambios a realizar al disco antes de realizarlos  

Superbloque: es una estructura de datos que contiene información sobre el sistema de archivos como dirección de la tabla de inodos, número de bloques libres y usados, etc  

fsck: (File System Check) Equivalente a chkdsk de windows. Chequea y repara sistemas de ficheros de linux (ext2, ext3), es necesario desmontar la partición  
fsck /dev/sdb1  
fsck -N /dev/sdb1 (para ver lo que haría, como una simulación)  

e2fsck -c /dev/sdb1 Hace un chequeo de bloques dañados en sistemas ext2  
e2fsck -f /dev/sdb1 Fuerza el chequeo aunque esté montado el disco, usar con precaución  

Comando xsf_info, muestra información sobre el sistema de archivos xfs  

xfs_metadump /dev/sdb2 archivo Guarda los metadatos de un sistema xfs en un fichero  

tune2fs: usado para editar configuraciones del sistema de archivos ext2 y ext3  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)