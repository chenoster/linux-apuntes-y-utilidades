## GESTIÓN DE PROCESOS

pstree Muestra el árbol de procesos  

Lifetime es el tiempo de vida de un programa  
Ambiente o entorno se refiere al entorno de variables y parámetros de la sesión actual  
Directorio actual de trabajo es el directorio donde trabaja un programa  

ps: (Process) para mostrar los procesos activos en el sistema.  
ps -a, muestra procesos de todos los usuarios.  
ps -f, muestra argumentos de los comandos.  
ps -l, formato grande, muestra información extendida.  
ps -u, muestra los usuarios que ejecutan los procesos.  
ps -x, muestra los procesos que no fueron ejecutados por una consola.  
ps -C cmd, muestra las instancias del comando cmd.  
ps -U usuario, muestra los procesos del usuario definido.  
ps -e | grep apache (ver si está en ejecución un programa)  

Comando pstree: muestra en orden jerárquico los procesos en el sistema.  
-a, muestra los argumentos de los programas.  
-h, resalta el ancestro del proceso actual.  
-p, incluye el PID de cada proceso en la salida del programa.  

Comando free: muestra información sobre la memoria del sistema.  
-b, -k, -m, muestran la información en bytes, kilobytes o megabytes.  
-t, muestra el total.  
-s segundos, actualiza el display en segundos.  

Comando uptime muestra el tiempo actual, cuánto tiempo el sistema tiene online, usuarios logueados.  

htop: Administrador interactivo de procesos por consola mejorado (más moderno que top)  

top: Administrador interactivo de procesos por consola (mejor usar htop si está instalado)  

init 0 (Se refiere al runlevel 0) Apaga el equipo  
init 6 (Se refiere al runlevel 6) Reinicia el equipo  

Comando top/htop para visualizar información de procesos y memorias en una interfaz de texto constantemente actualizada.  

Los procesos en *nix pueden tener varios estados, con el comando top/htop nos indica su estado la columna S (status):  

D (Uninterruptible sleep): Espera ininterrumpible, probablemente está esperando una operación de E/S de algún dispositivo.  

R (Running): El proceso se encuentra corriendo en el procesador.  

S (Interruptible sleep): Espera interrumpible, en espera a que se cumpla algún evento.  

T (Stopped): Detenido, generalmente mediante el envío de alguna señal.  

Z (Defunct o Zombie): Proceso terminado cuyo padre aún sigue vivo y no ha capturado el estado de terminación del proceso hijo y por lo tanto no lo ha borrado de la tabla de procesos.  
BACKGROUND: aquellos procesos que se encuentran en ejecución y no son mostrados directamente en la terminal.  
CONTROL+z o usando & después del comando se envía un proceso a background  

jobs nos muestra todos los procesos que están en background  
jobs -l nos muestra también su PID  

FOREGROUND: es un proceso con el que se puede interactuar directamente en una terminal  
fg trae el primer proceso del buffer a foreground  

nohup: para ejecutar comandos inmunes a mensajes provenientes de una terminal que seguirán en ejecución aunque cerremos la sesión.  

SEÑAL DE PROCESO: son mensajes entre los procesos que pueden ser enviados por el kernel o el usuario para que el programa realice una acción  

Tipos de señales:  
HUP 1, hangup es usada por demonios para recargar sus configuraciones sin reiniciar el proceso. También puede es usada cuando un módem cuelga  
INT 2, interrupción, usado cuando digitamos control+c en la terminal  
KILL 9, mensaje para detener un proceso  
TERM 15, terminar un proceso normalmente  
TSTP 20, usado para suspender la ejecución temporalmente con control+z  
CONT 18, usado para continuar la ejecución  

Comando kill, para enviar señales a los procesos  
-l, muestra las señales que podemos enviar  

Comando killall, para enviar señales a procesos con nombres  
-I, ignorar case  
-l, mostrar todas las señales  
-s, enviar una señal en particular  
-q, no mostrar output  

xkill: Para matar procesos haciendo click sobre la ventana del programa  

PRIORIDAD DE PROCESO: es el nivel que define cuáles procesos pueden usar más recursos que otros.  
Rango de prioridades: -20 hasta +19. Mientras menor el valor mayor la prioridad.  

Comando nice: para definir la prioridad de ejecución de un nuevo proceso.  
-n número, donde número define el nivel de prioridad.  
-número, donde número define el nivel de prioridad.  

Comando renice: usado para cambiar la prioridad de ejecución de un proceso en memoria.  
-u username, cambiar la prioridad de todos los procesos de un usuario.  

*Importante saber que nice -19 nos indica una prioridad de +19, si queremos indicar -19 entonces es nice --19  
Es mejor escribirlo así: nice -n -19 para evitar confusión  

renice 19 Para cambiar la prioridad del proceso  

Sin ser root sólo nos dejará cambiar entre el 0 y el 19  

nice: para definir la prioridad de ejecución de un nuevo proceso. (-20 a +19)  
nice -n número, donde número define el nivel de prioridad.  
nice -número, donde número define el nivel de prioridad.  

renice: usado para cambiar la prioridad de ejecución de un proceso en memoria. (Prioridad va de -20 hasta +19)  
renice -u username, cambiar la prioridad de todos los procesos de un usuario.  
renice 19 Para cambiar la prioridad del proceso, sin root sólo nos dejará del 0 al 19  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)