### EXPRESIONES REGULARES

. Cualquier carácter  
^ Inicio de la cadena  
$ Final de la cadena  
| OR  
\ Escapar carácter  
[] Rango  
() Agrupar lógicamente  

Operadores de cantidades  

Cero o más  
Uno o más  
? Cero o uno  
{m,n} Cuantificador manual  
Rangos especiales:  
:alnum: Alfanumérico  
:alpha: a-zA-Z  
:digit: Números de 0-9  
:blank: Espacios y tabuladores  
:lower: Minúsculas  
:upper: Mayúsculas  
:space: Espacios  

Comando grep: usado para buscar coincidencias en archivos de texto o en stdin usando un patrón de búsqueda.  
-c, muestra el número de líneas encontradas.  
-h, muestra las líneas que coinciden con la búsqueda.  
-i, no es sensible a mayúsculas o minúsculas.  
-n, muestra líneas que coinciden con la búsqueda y sus números.  
-v, muestra líneas que no tuvieron coincidencias.  
-E, interpreta el regex en formato extendido. Es lo mismo que egrep. 
-F, no interpreta regex, busca varias cadenas de texto en archivos. Es lo mismo que el comando fgrep.  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)