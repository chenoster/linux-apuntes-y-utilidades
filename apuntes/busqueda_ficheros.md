### BÚSQUEDA DE FICHEROS

find: Busca ficheros  
find -name "nombre_archivo"  
find -name "nombre_*"  
find -typr d (directorios)  
find -type f (file, archivos)  
find -type f -maxdepth 1 (nivel de profundidad)  
find -perm 776 (buscar por permisos)  
find -perm 776 -exec rm {} ; (borramos los ficheros encontrados)  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)