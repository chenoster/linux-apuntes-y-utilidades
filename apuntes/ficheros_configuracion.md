## FICHEROS DE CONFIGURACIÓN

Scripts que son llamados al ejecutar la terminal:  

/etc/profile, ejecutado por todos los usuarios al hacer login  
/etc/bashrc, generalmente ejecutado por el bashrc del usuario  
~/.bash_profile, es ejecutado despues de /etc/profile si existe  
~/.bash_login, ejecutado dsi bash_profile no existe  
~/.profile, ejecutado si no existe bash_profile ni bash_login  
~/.bashrc, ejecutado al iniciar una shell no interactiva  
~/.bash_logout, ejecutado al salir de la terminal  
~/.inputrc, contiene scripts para configurar el comportamiento del teclado  

Rutas de almacenamiento de configuraciones de X:  
/etc/X11/xorg.conf  
/usr/share/X11/xorg.conf.d/  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)