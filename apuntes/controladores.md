### MÓDULOS Y DRIVERS

modinfo: Para obtener información de algún modulo del sistema .ko  

modprob -i Para activar un modulo de sistema y todas sus dependencias  
modprob -r Para desactivar un modulo de sistema  

insmod Para activar un modulo del sistema (mejor modprob -i)  
rmmod para desactivar un modulo del sistema (mejor modprob -r)  

Para ver dispositivos y driver que controla cada uno de ellos:  
lspci -v   
usb-devices

Para cargar un driver al arrancar (ejemplo módulo v4l2loopback):
~~~
/etc/modules
Añadir linea: 
v4l2loopback

/etc/modules-load.d/
Añadir fichero v4l2loopback.conf con el texto: 
options v4l2loopback devices=1 card_label="loopback 1" exclusive_caps=1,1,1,1,1,1,1,1 

cd /etc/modprobe.d/
sudo cp /etc/modules-load.d/v4l2loopback.conf .

update-initramfs -c -k $(uname -r)
~~~

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)