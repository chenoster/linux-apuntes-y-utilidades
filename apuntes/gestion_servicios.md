## GESTIÓN DE SERVICIOS

Runlevels: Modos de operación del sistema operativo (En sistemas *nix)  
0 Apagado. Scripts que matan todos los procesos y apagan el equipo (se llama con init 0)  
1 Single user mode: Para hacer un debug o mantenimiento  
2 Modo multiusuario (En debian y X, red hat no).  
3 Modo multiusuario en red hat (red, ...). En otras puede no ser usado este runlevel  
4 Generalmente no es usado aunque si se desea se pueden añadir scrips manualmente  
5 En red hat se inicia el sistema de ventanas X  
6 Reiniciar equipo (se llama con init 6)  

Proceso init (Es el primer proceso que llama a los demás)  

Los ficheros de configuración pueden estar en:  
/etc/rc.sysinit (Red hat)  
/etc/init.d/rcS (Debian)  

/etc/rc.local Es llamado cuando se han ejecutado los demás  

Los archivos de configuración de todos los servicios que tenemos están en:  
/etc/init.d/  

Scripts que son ejecutados por cada runlevel:  
/etc/rc2.d/ (Para runlevel 2)  

/etc/init.d Scripts para manejar los servicios (start, stop, restart, reload, status) Sin parámetros nos dirá cuales son las opciones  

Los scripts dentro de un runlevel tienen una nomenclatura particular (K kill para los procesos que serán matados, S start para los procesos que serán iniciados)  

update-rc.d script defaults (Es normal que salgan mensajes de error) Esto se hace para actualizar las dependencias de scripts después de realizar algún cambio  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)