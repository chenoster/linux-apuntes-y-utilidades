## GESTIÓN DE PAQUETES

alien: Conversor de paquetes para instalar en una distribución que no es la del paquete original (deb-rpm, deb-tar, rpm-deb, ...)  

apt-get: Administrador de paquetes en Debian y derivados (requiere root)  
apt-get install Para instalar paquetes y sus dependencias  
apt-get remove Desinstala paquete pero deja dependencias y ficheros de configuración  
apt-get upgrade Actualiza todos los paquetes con nueva versión disponible  
apt-get autoclean  
apt-get autoremove  
apt-get dist-upgrade  

pacman: Gestor de paquetes de Arch Linux (requiere root)  
pacman -S Para instalar o actualizar paquetes y sus dependencias. Se puede especificar antes del nombre del paquete: extra/paquete o testing/paquete  
pacman -Si Muestra amplia información sobre un paquete  
pacman -Syu Para actualizar el sistema  
pacman -Q (Query) Lista todos los paquetes instalados, también se puede buscar un paquete en concreto para ver si lo tenemos instalado y qué versión es  
pacman -Qs Lista paquetes instalados con una pequeña descripción, también se puede especificar un paquete concreto  
pacman -Qi Nos ofrece amplia información sobre un paquete instalado  
pacman -Ql Nos muestra una lista de los archivos instalados por el paquete  
pacman -Qo <ruta_archivo> También podemos saber a qué paquete pertenece un archivo del sistema  
pacman -Qdt Lista todos los paquetes que no se necesitan como dependencias (huérfanos)  
pacman -R Elimina el paquete individual dejando sus dependencias  
pacman -Rs Borra el paquete y sus dependencias que no sean utilizadas por otros paquetes  
pacman -Rn (o -Rns) Elimina el paquete y los ficheros de configuración (pacman guarda una copia de los ficheros de configuración añadiendo la extensión .pacsave)  
pacman -Ss Para buscar un paquetes en los repositorios. Sin especificar un paquete nos saldrá el listado completo de paquetes disponibles  
pacman -Sw Descargar un paquete sin instalarlo  
pacman -U <ruta_paquete.pkg.tar.xz> Instala un paquete local o remoto (la ruta puede ser una url)  
pacman -Sc Limpia la caché de paquetes que ya no están instalados (/var/cache/pacman/pkg)  
pacman -Scc Limpia la caché de paquetes completa  
alias pSyu='sudo pacman -Syu'  

rpm de Red Hat y derivados:  
nombre_paquete-VVV-RRR.AAA.rpm (versión, revisión y arquitectura)  

rpm:  
-i --install, instalar paquetes.  
--force, reemplazar paquetes.  
-h, mostrar indicador de progreso.  
--test, probar si el paquete puede instalarse antes de hacer los cambios.  
-v, modo debug.  
-U --upgrade, actualizar paquetes.  
-e --unistall, desinstalar un paquete  
--nodeps, saltar la verificación de dependencias.  
--test, prueba borrar los archivos sin realmente borrarlos.  
-q --query, buscar / hacer query  
-a, mostrar todos los paquetes instalados.  
-f archivo, mostrar los paquetes al que pertenece un archivo.  
-i, usar paquetes del sistema.  
-l, listar los archivos que tiene un paquete.  
-R, mostrar dependencias de un paquete.  
-V --verify, verificar si un paquete ha sido modificado  

rpm2cpio: Para editar paquetes, por ejemplo, para cambiar una configuración  
rpm2cpio > archivo.cpio  
cpio -idmv < archivo.cpio (descomprimimos y desempaquetamos)  

Descripción del comando yum.  

Archivos relevantes:  
/etc/yum.conf  
/etc/yum.repos.d  

Parámetros de yum:  
install, instalar un paquete.  
erase, elimina paquetes.  
remove, elimina paquetes y sus dependencias.  
check-update, verifica si existen nuevas versiones de programas  
update, actualiza los paquetes en el sistema.  
list, lista un paquete o grupo de paquetes.  
info, mostrar informaciones de un paquete.  
search, busca un paquete dado una cadena de texto.  

Comando yumdownloader: Descargar sin instalar  
--resolve, para descargar dependencias  

deb de Debian y derivados:  
nombre_paquete_VVV-RRR_AAA.deb  

dpkg -l (--list) lista paquetes instalados  
dpkg -L (--listfiles) lista archivos instalados por un paquete  
dpkg -p (--print-avail) muestra información detallada de un paquete  
dpkg S --search, busca un archivo en paquetes instalados.  
dpkg -i --install, para instalar un paquete.  
dpkg -r --remove, remover un paquete.  
dpkg -P --purge, para remover un paquete completamente.  
dpkg-reconfigure para configurar de nuevo algún paquete instalado.  
dselect, para gestionar paquetes.  
alien, para convertir paquetes a diferentes formatos.  
alien -i, para instalar un paquete  
alien -r, para convertir el paquete a .rpm  
alien -t, para convertir a tar.gz  

apt-get:  

Uso de repositorios online:  
/etc/apt/sources.list  

Parámetros comunes:  
update, actualizar la lista de paquetes usando un repositorio  
install, instalar nuevos paquetes  
remove, eliminar paquetes  
upgrade, actualizar los programás antiguos  
dist-upgrade, actualizar el sistema operativo  
Descripción del comando apt-cache, consultas en el caché local  

apt-cache:  

Parámetros comunes:  
search, buscar en el cache  
show, mostrar información detallada de un paquete  
depends, mostrar dependencias  
rdepends, mostrar dependencias recursivas  

aptitude:  

Parámetros comunes:  
search, buscar en el cache paquetes por el nombre.  
install, instalar paquetes.  
remove, remover paquetes.  
purgue, eliminar paquetes y sus configuraciones.  
update, actualizar la lista de paquetes.  
reinstall, reinstalar un paquete.  
safe-upgrade, actualiza paquetes sin eliminar ninguno.  
clean, elimina los archivos .deb descargados en el cache.  
autoclean, elimina los archivos .deb del caché de versiones anteriores de software.  

Eliminar kernel antiguo en Fedora:  
rpm -qa kernel (para ver los kernel que tenemos instalados)  
sudo dnf remove kernel-...... (así eliminamos el que queramos)  
sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg (reconfiguramos el grub)  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)