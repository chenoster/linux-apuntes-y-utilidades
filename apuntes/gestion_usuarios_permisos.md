## GESTIÓN DE USUARIOS Y PERMISOS

4 2 1  
R W X  

Bits o permisos especiales:  
suid: permite ejecutar un archivo con los permisos del dueño  
guid: permite ejecutar un archivo con los permisos del grupo  
sticky: deja el programa en memoria o permite que sólo el dueño de un archivo pueda eliminarlo o cambiarle el nombre  

chmod: (Change mode) Sirve para modificar permisos de ficheros y carpetas  
chmod 600 archivo  
chmod g+rwx archivo  
chmod u+x archivo  
chmod g-x archivo  
chmod 1600 archivo (para sticky, se muestra como una T)  
chmod o+t archivo (para sticky, se muestra como una T)  
chmod 2600 archivo (suid, S)  
chmod 4600 archivo (guid, S)  
chmod 777 -R archivo (cambia permisos de manera recursiva)  

chroot: (Change root) Cambia el directorio raíz (jaula root), se utiliza para instalar el sistema de manera manual desde una distro live  

chown: (Change Owner) Cambia propietario del fichero o directorio  
chown pepe archivo  
chown pepe:grupopepe archivo  
chown pepe.grupopepe archivo (en bsd)  
chown -R (para cambiar de usuario de manera recursiva)  

chgrp root archivo: Cambia el grupo del archivo  

umask: Permisos por defecto al crear un archivo o directorio, normalmente está definido en /etc/profile o .bashrc  
umask realiza una resta entre 777 y los permisos que le pasamos como parámetro, por tanto, si queremos permiso 777 tenemos que poner 000  
umask 555: Los permisos por defecto en este caso serían 222 (sólo escritura para todos)  

edquota: (Edit Quota) Editar la cuota de un usuario o grupo (cuota de disco, tamaño y número de ficheros)  
Existe un límite blando y un límite duro (hard limit y soft limit)  
El límite blando te avisa una vez sobrepasado y tiene un periodo de gracia, normalmente de 7 días  
El límite duro, si se sobrepasa ya no permite escribir más  
Hay que instalar los paquetes quota y quotatool y activarlo con modprobe quota_v1  
Además hay que montar la partición con los parámetros de usrquota y/o grpquota  
Las quotas se definen por sistema de archivos y no por carpeta  

quotacheck: Crea un archivo compilado que guarda información de todos los archivos, inodos, etc que tiene cada usuario.  
quotacheck -maguv Para que monte y compruebe todos los sistemas de ficheros y genere los ficheros de grupo y de usuario en modo verbose  
quotaon -aguv Para que active todas las quotas de grupo y de usuario  
quotaoff -aguv Para que desactive todas las quotas  
Conviene desactivar las quotas antes de hacer quotacheck, luego volvemos a activar  
quota -u usuario: Para ver las quotas definidas para un usuario  
quota -g grupo: Para ver las quotas definidas para un grupo  
repquota -augv: Nos muestra un reporte sobre el uso de quotas de los últimos 7 días  

whoami: (¿Quién soy yo?) Muestra el usuario con el que estamos conectados  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)