## SISTEMA DE DIRECTORIOS

Pueden ser compartidos o no compartidos (sharable o non sharable) y modificables o no (variable o estático).  

/ Directorio raíz o root de donde cuelgan todos los demás  
/etc Ficheros de configuración  
/var Ficheros variables, que van cambiando  
/var/log Ficheros de log  
/var/mail Aquí se almacenan los correos electrónicos  
/var/lock Ficheros de bloqueo de ciertas aplicaciones  
/tmp Directorio temporal, acceso público normalmente  
/boot Directorio de arranque, aquí se almacena el kernel de linux y algunos ficheros de configuración  
/home Directorio donde se guardan las carpetas de usuario  
/proc Procesos que tenemos en memoria  
/sys Características del sistema o del kernel  
/dev Dispositivos que tenemos conectados  
/opt Directorio donde se guardan los programas opcionales que no son propios de la distribución  
/root Carpeta del usuario root, archivos que permiten al sistema levantarse y herramientas administrativas  
/usr Programas de usuario  
/etc/rcX.d runlevel X  
/sbin Binarios de administrador  
/bin Binarios de sistema esenciales que pueden ser ejecutados por cualquier usuario  
/lib Librerías y módulos  
/usr Programas instalados por los usuarios  
/usr/bin Binarios de usuario no esenciales  
/usr/local/sbin Binarios instalados en local (no vienen de los repositorios) del usuario root  
/usr/local/bin Binarios instalados localmente (no vienen de los repositorios)  
/mnt Donde se montan por defecto los discos duros  
/media Donde se montan los dispositivos de almacenamiento extraible  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)