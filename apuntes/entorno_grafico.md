## ENTORNO GRÁFICO

Rutas de almacenamiento de configuraciones de X:  
/etc/X11/xorg.conf  
/usr/share/X11/xorg.conf.d/  

Crear un archivo de configuración automático con:  
X -configure  

Formato de un archivo de xorg:  
ServerLayout  
Files  
Module  
InputDevice  
Monitor  
Device  
Screen  

Comprobar compatibilidad de la placa de video con el sistema.  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)