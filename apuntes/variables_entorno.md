## VARIABLES DE ENTORNO

Éstas son algunas de las variables más comunes, pero puede haber muchas más:  

DISPLAY Donde aparecen las salidas de X-Window  
HOME Directorio personal del usuario  
HOSTNAME Nombre de la máquina  
MAIL Ruta de la bandeja de entrada de correo  
PATH Lista de directorios donde se encuentran los programas (binarios)  
PS1 Prompt de la consola. Se puede editar en ~.bashrc  
SHELL Intérprete de comandos por defecto  
TERM Tipo de terminal  
USER Nombre de usuario  
PWD Directorio actual  
OLDPWD Directorio anterior  
LANG El típico es_ES.UTF8  
_ Último comando ejecutado  
HISTSIZE Número máximo de entradas en el historial de comandos  
HISTFILESIZE Tamaño máximo del historial de comandos  
USERNAME Nombre de usuario  

echo $VARIABLE: Muestra el valor de la variable  

env: Lista todas las variables de entorno  

export: Lista todas las variables de entorno  
export PATH=$PATH:/home/cheno/scripts: Coloca en memoria una variable, sólo permanece en esa sesión  
export VARIABLE=: Para borrar el valor de una variable  

printenv: Lista todas las variables de entorno  
printenv VARIABLE: Muestra el valor de la variable  

set -o: Nos muestra opciones de funcionamiendo de nuestra shell  
set -o vi: Activa el parámetro vi de la configuración de la shell  

unset VARIABLE: Para eliminar una variable  

VARIABLE DE ENTORNO PERMANENTE  

Podemos hacer que una variable sea permanente para todos los usuarios o para un usuario en concreto, para ello debemos añadir el export a alguno de los siguientes ficheros de configuración:  

Para todos los usuarios:  
/etc/environment (Si existe en nuestro sistema operativo, no en todos los linux se encuentra). Sería el lugar más apropiado. En este caso se incluye VARIABLE=VALOR (Sin el export)  
/etc/profile  
/etc/bashrc (No siempre existe, normalmente se utiliza el que está en la carpeta del usuario ~/.bashrc)  

Para un usuario específico: ~/.bash_profile ~/.bash_login ~/.profile (si no existen los dos anteriores) (y yo añadiría ~/.bashrc)  

Al salir de la terminal se ejecuta el fichero ~/.bash_logout  

~/.inputrc puede existir este archivo que contiene scripts para configurar el comportamiento del teclado  

El orden de carga es el mismo en el que se han nombrado los ficheros.  

La variable PATH se exporta de la siguiente manera para añadir una ruta manteniendo las que ya existen:  
export PATH=nueva_ruta:$PATH  

Como vemos, si la variable tiene varios valores, éstos se separan con :  

Si el valor de la variable tiene espacios entonces se pone entre comillas  

export VARIABLE="valor con espacios"  

set -> Con el comando set vemos un listado bastante extenso de variables y funciones de la shell  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)