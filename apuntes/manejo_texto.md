## MANEJO DE CADENAS DE TEXTO

cat: (Concat) Concatena ficheros, aunque se usa habitualmente para mostrar el contenido de un fichero por la salida estándar  

Comando cut: separa el archivo en columnas y permite mostrarlas separadamente.  
-ddelim, utiliza el delimitador delim  
-clist, imprime el carácter definido en list  
-flist, imprime los campos definidos en list  
-blist, imprime los bytes definidos en list  

sort: Ordena por orden alfabético  
ls | sort  
sort -r (reverse) Ordena en orden inverso  
sort -k 4 (Ordena utilizando una columna en particular)  
sort -n (numeric) Ordena numericamente  

echo: Muestra por pantalla un texto o el valor de una variable  

tr: (traslate) Realiza una "traducción" de un juego de caracteres a otro juego de caracteres  
cat | tr a-z A-Z -> Pasa de minúsculas a mayúsculas  
cat | tr -d '[:blank:]' -> Elimina los espacios en blanco  
cat | tr -s '[:blank:]' -> Elimina los espacios (u otro caracter que le indiquemos) repetidos  

od: Transforma todo el juego de caracteres hacia otro, para convertir a octal o hexadecimal  
od -t x -> Imprime el archivo en hexadecimal (x hexadecimal, o octal, c ascii)  

split: Sirve para dividir fichero, por defecto divide en archivos de 1000 lineas  
split -n 10 -> Divide en ficheros de 10 lineas cada uno con nombre "nombre archivos salidaaa", "nombre arvhivos salidaab", ...  

uniq: Elimina los resultados duplicados que son adyacentes  
cat | sort | uniq -> Para solucionar lo de los adyacentes primero ordenamos  
uniq -u Mostramos sólo las lineas únicas  
uniq -d Muestra sólo las lineas repetidas  

wc: (word count) Podemos contar lineas, palabras o caracteres (por defecto muestra las 3 cosas, salen 3 números) (parámetros -l -c -w)  

head (Cabecera) Muestra las 10 primeras lineas del fichero  
head -cX Muestra los X primeros bytes del archivo  
head -nX Muestra las X primeras lineas del archivo  

tail Muestra las últimas 10 lineas del fichero de texto  
tail -cX Muestra los últimos X bytes del fichero  
tail -nX Muestra las últimas X lineas del fichero  
tail -f Muestra las últimas lineas del fichero de manera interactiva, perfecto para loguear  

fmt: (Format) Formatea un fichero de texto en columnas  
fmt -w 2 -> Formatea en dos columnas  
fmt -uw 2 -> Espaciado uniforme  

join: Une dos ficheros por un campo en común, como si fueran tablas de una base de datos  
join -j 1 id_personas personas_coches -> -j 1 indica que los campos por los que queremos unir están en la primera posición  

sed: Comando que permite hacer cambios en uno o varios ficheros a través de expresiones regulares. (Ver manual_sed)  
sed s/mensaje/casa1/ -> Sustituimos el texto mensaje por casa1 la primera vez que lo encuentre s (Sustitución)  
sed s/mensaje/casa1/gi -> g hace que se sustituya mensaje en todo el texto, no sólo la primera ocurrencia. i hace que sea Insensible a mayúsculas/minúsculas  
sed -n -> Parametro para no mostrar los cambios en la pantalla  
sed -i -> En los casos anteriores sólo se veían los cambios en pantalla pero no habíamos modificado el fichero. Con el parámetro i se hacen los cambios en el fichero  

nl (number line) Enumera las lineas que contienen texto  
nl -b a -> Enumera todas las lineas aunque no contengan texto  
nl -b t -> Sólo las que contienen texto  
nl -b n -> No numera ninguna linea (b body, h header, f footer)  

pr: (print) Formatea texto para imprimir. Inserta cabecera y margen 
pr -d -> Doble espaciado  
pr -o -> Define el tamaño del margen izquierdo en número de caracteres  

wc: (Word Count) Cuenta palabras de un fichero o de otro comando que le pasamos como entrada (ver otros usos según parámetros)  
wc -l (Lines) Cuenta número de lineas  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)