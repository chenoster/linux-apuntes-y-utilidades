## MANUAL DE AWK

AWK es una especie de navaja suiza que nos provee de herramientas para el manejo de archivos de texto. 
La sintaxis básica es la siguiente:

awk patrón { acción } archivo

Se puede omitir el patrón o la acción pero no ambos. La acción puede ser un script guardado en un fichero y el archivo de entrada pueden ser varios archivos.

awk lee los ficheros de entrada línea a línea buscando el patrón, cuando lo encuentra ejecuta la acción.

Se puede guardar un programa awk en un fichero a modo de script de bash:  
~~~
#!/bin/awk -f

BEGIN { print "Hola, Mundo!" }
~~~

awk -F: '$2==""' /etc/passwd

Usamos -F para fijar el separador de campos, con este comando obtendremos las líneas de usuarios que no tienen contraseña

awk 'patrón { acción } archivo'  
comando | awk 'patrón { acción }'  
awk -F script_awk archivo  
awk 'patrón' archivo  
awk '{ acción }' archivo  


[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)