## GESTIÓN DE ARRANQUE

BIOS Interface de más bajo nivel que se puede comunicar con el hardware y llama al bootloader  

Bootloaders: Es una pequeña rutina o programa  
/boot  
initrd: Imagen básica del file system para que el kernel pueda desarrollar todas sus funciones  
vmlinuz: kernel de linux  

Proceso de booteo (pueden existir diferentes pasos pero en general es así):  

BIOS comprueba el hardware y dispositivos  
BIOS verifica que existe un MBR en el disco duro y lo ejecuta. Un MBR (Master Boot Record) Es una pequeña sección del disco duro, 512 bytes, en los primeros sectores, que almacena el bootloader, al menos su parte principal  
Se ejecuta el bootloader  
/boot Si el bootloader necesita más módulos los busca en /boot  
initrd Pequeña imagen del filesystem que se levanta al final del paso anterior  
Levantar el kernel de linux  
El bootloader es cerrado y todos los permisos del bootloader pasan hacia el kernel  
Bootloaders: grub y lilo  
Cuando el kernel de linux es iniciado se le pueden pasar parámetros  

Proceso de booteo con grub:  

Single user mode: Te permite no cargar ningún otro usuario ni programas externos al inicia, para reparar problemas en el booteo.
Apretamos la tecla e para editar cuando iniciemos la máquina y salga el menú del grub. Buscamos la linea donde aparece la ruta del kernel y añadimos single al final  
Fundamentos de GRUB  
/boot/grub/grub.cfg Archivo de configuración que no debería ser editado a mano, se genera con un comando a partir de otros ficheros.  
Podemos ver las entradas del menú y los módulos que son inicializados junto con el kernel, también los discos duros y partición (los números de partición en este caso empiezan en 1)  
/etc/grub.d Aquí están los ficheros de configuración que si pordemos tocar  
00_header Parámetros generales como timeout, entrada default, ... 
05_debian_theme Pantalla principal de bienvenida  
10_linux Configuraciones particulares de la distro  
10_linux_xen " "  
30_os-prober Carga todas las entradas de los distintos sistemas operativos que se encuentren en el disco duro  
40_custom Templates para poder crear nuestras propias configuraciones  
41_custom " "  

Los que empiezan por 00 se ejecutan primero  

Si se quiere desactivar alguno podemos quitar el bit de ejecución (permisos de ejecución x)  

/etc/default/grub En este archivo podemos modificar algunos parámetros como el tiempo de espera, elemento del menú por defecto, ...  

Para crear una nueva entrada del menú, copiamos el fichero 40_custom  
cp 40_custom 43_custom_mientrada  

Para actualizar el fichero de configuración principal: update-grub  
Para instalar grub: grub-install /dev/sda  

fdisk -l (Para ver si el disco duro tiene el flag de boot marcado)  

Runlevels: Modos de operación del sistema operativo (En sistemas *nix)  
0 Apagado. Scripts que matan todos los procesos y apagan el equipo (se llama con init 0)  
1 Single user mode: Para hacer un debug o mantenimiento  
2 Modo multiusuario (En debian y X, red hat no).  
3 Modo multiusuario en red hat (red, ...). En otras puede no ser usado este runlevel  
4 Generalmente no es usado aunque si se desea se pueden añadir scrips manualmente  
5 En red hat se inicia el sistema de ventanas X  
6 Reiniciar equipo (se llama con init 6)  

Proceso init (Es el primer proceso que llama a los demás)  

Los ficheros de configuración pueden estar en:  
/etc/rc.sysinit (Red hat)  
/etc/init.d/rcS (Debian)  

/etc/rc.local Es llamado cuando se han ejecutado los demás  

Los archivos de configuración de todos los servicios que tenemos están en:  
/etc/init.d/  

Scripts que son ejecutados por cada runlevel:  
/etc/rc2.d/ (Para runlevel 2)  

/etc/init.d Scripts para manejar los servicios (start, stop, restart, reload, status) Sin parámetros nos dirá cuales son las opciones  

Los scripts dentro de un runlevel tienen una nomenclatura particular (K kill para los procesos que serán matados, S start para los procesos que serán iniciados)  

update-rc.d script defaults (Es normal que salgan mensajes de error) Esto se hace para actualizar las dependencias de scripts después de realizar algún cambio  

[Volver al índice](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/apuntes/README.md)
