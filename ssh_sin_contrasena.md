## Conecta por SSH de manera segura sin que escribir la contraseña

Si no quieres escribir la contraseña cada vez que conectas por ssh, o quieres utilizar un script para automatizar ciertas tareas y que la conexión siga siendo segura, puedes hacerlo con un par de claves pública/privada.

Para ello hay que seguir dos sencillos pasos (todo se hace en el pc desde el que vamos a conectar):

1) Generar un par de claves.

`ssh-keygen -b 4096 -t rsa`

Pulsamos enter para aceptar los valores por defecto.

2) Ahora le pasamos la clave pública al equipo al que vamos a conectar con el siguiente comando:

`ssh-copy-id usuario@ip_equipo_remoto`

(Si es para un script que requiere permisos de administrador habrá que pasarle la clave pública al usuario root: ssh-copy-id root@ip_equipo_remoto)


La próxima vez que nos conectemos no nos pedirá la contraseña. 

*Gracias a Paco por la idea.*