· IP Pública:   
`curl ifconfig.me`

· Dirección MAC:  
`cat /sys/class/net/eth0/address`  
`ip link show eth0`  
`ip link show eth0 | awk '/ether/ {print $2}'`

· Ejecutar un comando al levantar una interface de red:
~~~
auto wlan0
iface wlan0 inet manual
    up COMMAND
~~~
(/etc/network/interfaces.d/)

· Automatización pulsaciones de teclado y ratón con python:  
https://recursospython.com/guias-y-manuales/pyautogui/